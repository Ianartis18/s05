from abc import ABC, abstractclassmethod

class Person (ABC):
	@abstractclassmethod
	def getFullName(self):
		pass

	def addRequest(self):
		return "Request has been added"

	def checkRequest(self):
		pass

	def addUser(self):
		pass

	def addMember(self):
		pass

# Employee Class
class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName 
		self._email = email
		self._department = department

	# getters of Employee class
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	# setters of Employee class
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName 

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department


	# implementing abstract methods in Employee class
	def	checkRequest(self):
		pass

	def addUser(self):
		return "User has been added"

	def login(self):
		return  f"{self._email} has logged in"

	def logout(self):
		return  f"{self._email} has logged out"

	def getFullName(self):
		fullName = self._firstName +" "+ self._lastName
		return fullName


# Team Lead class
class TeamLead(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName 
		self._email = email
		self._department = department
		self._memberList = []

	# getters of Team Lead class
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	def get_members(self, ):
		return self._memberList 


	# setters of Team Lead class
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName 

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department



	# implementing abstract methods in Team Lead class
	def	checkRequest(self):
		pass

	def addUser(self):
		return "User has been added"

	def login(self):
		return  f"{self._email} has logged in"

	def logout(self):
		return  f"{self._email} has logged out"

	def getFullName(self):
		fullName = self._firstName +" "+ self._lastName
		return fullName

	def addMember(self,member):
		self._memberList.append(member)
		return self._memberList



		


# Admin Class
class Admin(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName 
		self._email = email
		self._department = department

	# getters of Admin class
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department

	# setters of Admin class
	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName 

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department


	# implementing abstract methods in Admin class
	def	checkRequest(self):
		pass

	def addUser(self):
		return "User has been added"

	def login(self):
		return  f"{self._email} has logged in"

	def logout(self):
		return  f"{self._email} has logged out"

	def getFullName(self):
		fullName = self._firstName +" "+ self._lastName
		return fullName

class Request():
	def __init__(self, name, requester, dateRequested):
		self._name = name
		self._requester = requester 
		self._dateRequested = dateRequested
		self._status = "open"

	# getters of Request class
	def get_name(self):
		return self._name

	def get_requester(self):
		return self._requester

	def get_dateRequested(self):
		return self._dateRequested

	def get_status(self):
		return self._status

	# setters of Request class
	def set_name(self, name):
		self._name = name

	def set_requester(self, requester):
		self._requester = requester 

	def set_dateRequested(self, dateRequested):
		self._dateRequested = dateRequested

	def set_status(self, status):
		self._status = status	

	# methods of Request class
	def updateRequest(self):
		print("Request updated")

	def closeRequest(self):
		return (f"Request {self._name} has been {self._status}")

	def cancelRequest(self):
		print("Request cancelledd")




# Test Cases:
employee1 = Employee("John","Doe","djohn@mail.com","Marketing")
employee2 = Employee("Jane","Smith","sjane@mail.com","Marketing")
employee3 = Employee("Robert","Patterson","probert@mail.com","Sales")
employee4 = Employee("Brandon","Smith","sbrandon@mail.com","Sales")
admin1 = Admin("Monika","Justin","jmonika@mail.com","Marketing")
teamLead1= TeamLead("Michael","Specter","smichael@mail.com","Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-d2021")
req2 = Request("Laptop repair", employee1, "1-Jul-d2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
	print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
